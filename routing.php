<?php

use App\Utils\Router;
use App\Controllers\HomeController;

Router::get('/', array( new HomeController() , 'index'));
Router::post('/', array( new HomeController() , 'upload'));