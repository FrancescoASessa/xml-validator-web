<?php
namespace App\Utils;

use Flight;

class Router
{
	public $file = __DIR__ . '/../../routing.php';

	function __construct($file = null)
	{
		if(!is_null($file)) $this->file = $file;

		require $this->file;

	}

	public static function get($path,$callback)
	{
		Flight::route("GET $path", $callback);
	}

	public static function post($path,$callback)
	{
		Flight::route("POST $path", $callback);
	}

}