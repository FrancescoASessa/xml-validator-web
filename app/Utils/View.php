<?php
namespace App\utils;

use \Flight;
/**
* View
*/
class View
{
	public $path = './public';
	public $extension = '.php';

	function __construct()
	{
		$this->setPath();
		$this->setExtension();
	}

	public static function render($file,$data = [],$global = [])
	{
		Flight::render('layout/header',$global);
		Flight::render($file,$data);
		Flight::render('layout/footer',$global);
	}

	private function setPath($path = null)
	{
		if(!is_null($path)) $this->path = $path;

		Flight::set('flight.views.path', $this->path);
	}

	private function setExtension($extension = null)
	{
		if(!is_null($extension)) $this->extension = $extension;

		Flight::set('flight.views.extension',$this->extension);
	}
}