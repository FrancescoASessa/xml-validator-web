<?php

namespace App\Utils;

use Flight;

class Application
{
	public $view;

	function __construct()
	{
		$this->view = new View();
		$this->router = new Router();
	}

	public function start()
	{
		return Flight::start();
	}
}