<?php

namespace App\Controllers;

use Flight;
use flight\net\Request;

class Controller
{
	public $request;

	function __construct()
	{
		$this->request = new Request();
	}


	public function response($data = [], $code = 200)
	{
		return  Flight::json($data,$code);
	}

	public function get($key)
	{
		return Flight::get($key);
	}
}