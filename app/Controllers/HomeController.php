<?php

namespace App\Controllers;

use App\Utils\View;
use XML\Validator;

class HomeController extends Controller
{
	
	public  function index()
	{
		View::render('index',['title' => 'Website title']);
	}

	public function upload()
	{
		$xmlFile = $this->request->files->xml;
		$xsdFile = $this->request->files->xsd;

		$xml = $this->processXML($xmlFile);

		$xsd = $this->processXSD($xsdFile);

		if($xsd === false && $xml === false){
			return $this->response([
				'status' => false,
				'message' =>  'An errors occurred',
				'errors' => [
					'Upload failed'
				]
			],500);
		}

		$validation = $this->validation($xml,$xsd);
		
		if(is_array($validation)){
			return $this->response([
				'status' => false,
				'message' =>  'An errors occurred',
				'errors' => $validation['errors']
			]);
		}else if($validation == true){
			return $this->response([
				'status' => true,
				'message' =>  "Feed successfully validated"
			]);
		}
		var_dump($validation);die;
	}


	private function processXML($xml){

		if($xml['error'] === 0){
			$tmp_name = $xml["tmp_name"];
			$name = basename($xml["name"]);
			$uploads_dir = $this->get('tmp_path') .'/'. $name;

			if(move_uploaded_file($tmp_name, $uploads_dir)){
				return $uploads_dir;
			}

			return false;
		}

		return false;
	
	}

	private function processXSD($xsd){

		if($xsd['error'] === 0){
			$tmp_name = $xsd["tmp_name"];
			$name = basename($xsd["name"]);
			$uploads_dir = $this->get('tmp_path') .'/'. $name;

			if(move_uploaded_file($tmp_name, $uploads_dir)){
				return $uploads_dir;
			}

			return false;
		}

		return false;

	}


	private function validation($xml,$xsd)
	{
		$validator = new Validator;
		$validator->setSchema($xsd);
		
		$validated = $validator->validate($xml);
		$errorsCounter = $validator->errorsCounter;

		unlink($xml);
		unlink($xsd);

		if ($validated && $errorsCounter == 0) {
			return true;
		} else {
			return [
				'count' => $errorsCounter,
				'errors' => $validator->errors
			];
		}
	}
}