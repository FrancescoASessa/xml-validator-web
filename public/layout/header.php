<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>XML Validator</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <!-- Bulma Version 0.6.0 -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.0/css/bulma.min.css" integrity="sha256-HEtF7HLJZSC3Le1HcsWbz1hDYFPZCqDhZa9QsCgVUdw=" crossorigin="anonymous" />
  <style type="text/css">
    html,body {
      font-family: 'Open Sans';
    }
    .results {
    	max-height: 320px;
    	overflow: auto;
    }
  </style>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-fork-ribbon-css/0.2.0/gh-fork-ribbon.min.css" />
<!--[if lt IE 9]>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-fork-ribbon-css/0.2.0/gh-fork-ribbon.ie.min.css" />
<![endif]-->
</head>
<body>
	<a class="github-fork-ribbon" href="https://gitlab.com/FrancescoASessa/xml-validator-web" title="Fork me on GitHub">Fork me on Gitlab</a>
	<section class="hero is-fullheight is-default is-bold">
	
		<!-- Header -->
		<div class="hero-head">
			<nav class="navbar">
				<div class="container">
					<div class="navbar-brand">
						<a class="navbar-item" href="../">
						XML Validator
						</a>
						<span class="navbar-burger burger" data-target="navbarMenu">
							<span></span>
							<span></span>
							<span></span>
						</span>
					</div>
					<!-- menu -->
					<div id="navbarMenu" class="navbar-menu">
						<div class="navbar-end"></div>
					</div>
				</div>
			</nav>
		</div>
		
		<!-- End Header -->