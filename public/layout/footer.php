
		<!-- Footer -->
	    <div class="hero-foot">
	      <div class="container">
	        <div class="tabs is-centered">
	          <ul>
	            <li><a href="https://francescosessa.com">Powered by Francesco Sessa</a></li>
	          </ul>
	        </div>
	      </div>
	    </div>
	    <!-- End Footer -->
	</section>
  <!-- Javascript -->
 <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script src="assets/js/app.js"></script>
</body>
</html>