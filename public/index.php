
		<!-- Body -->
	    <div class="hero-body">
			<div class="tile is-ancestor">
			  <div class="tile is-4 is-vertical is-parent">
			    <div class="tile is-child box">
			      <p class="title">XML</p>
			      <hr></hr>
			      <div class="content">
					<div class="field">
					  <div class="file is-centered is-boxed is-info has-name">
					    <label class="file-label">
					      <input class="file-input" type="file" id="xmlfile" accept=".xml">
					      <span class="file-cta">
					        <span class="file-icon">
					          <i class="fa fa-upload"></i>
					        </span>
					        <span class="file-label">
					          Upload file...
					        </span>
					      </span>
					      <span class="file-name xml-file-name"></span>
					    </label>
					  </div>
					</div>
			      </div>
			    </div>
			    <div class="tile is-child box">
			      <p class="title">XSD Schema</p>
			      <hr></hr>
			      <div class="content">
					<div class="field">
					  <div class="file is-centered is-boxed is-info has-name">
					    <label class="file-label">
					      <input class="file-input" type="file" id="xsdfile" accept=".xsd">
					      <span class="file-cta">
					        <span class="file-icon">
					          <i class="fa fa-upload"></i>
					        </span>
					        <span class="file-label">
					          Upload file...
					        </span>
					      </span>
					      <span class="file-name xsd-file-name"></span>
					    </label>
					  </div>
					</div>
			      </div>
			    </div>
			  </div>
			  <div class="tile is-parent">
			    <div class="tile is-child box">
			      <p class="title">Results <a class="delete is-pulled-right is-invisible"></a></p>

			      <div class="content results" id="results">
						
			      </div>
			    </div>
			  </div>
			</div>
	    </div>
		<!-- End Body -->
