/** 
 * Main JS File
 */

$(function() {

	let xmlfile = $("#xmlfile")
	let xsdfile = $("#xsdfile")
	let results = $("#results")
	let clearResults = $('.delete')

	let xmlSelected = false
	let xsdSelected = false

	xmlfile.change(function(event) {
		if(this.files.length > 0) {
			$('.xml-file-name').text(this.files[0].name)
			xmlSelected = true;
			submit()
		}
	});

	xsdfile.change(function(event) {
		if(this.files.length > 0){
			$('.xsd-file-name').text(this.files[0].name)
			xsdSelected = true;
			submit()
		}
	});

	clearResults.click(function(event) {
		event.preventDefault()
		results.text('')
	});

	function submit() {
		if(xmlSelected && xsdSelected){
		    let xml = xmlfile.prop('files')[0];   
		    let xsd = xsdfile.prop('files')[0];

		    var formData = new FormData();                  
		    formData.append('xml', xml);
		    formData.append('xsd', xsd);


		    $.ajax({
				url: '/',
				type: 'POST',
				data: formData, 
				dataType: 'json',
				cache: false,
				contentType: false,
				processData: false,
				success: function(response){
				    let status = response.status
				    let message = response.message

				    results.text('')

				    if(status == true){
				    	results.text(message)
				    	clear()
				    }else{
				    	let errors = response.errors
				    	$('<ol>').attr('id', 'list').appendTo(results)

				    	for (var i = errors.length - 1; i >= 0; i--) {
				    		let message = errors[i]
				    		$('<li>').text(message).appendTo('#list')
				    		clear()
				    	}
				    }
				}
			});
		}
	}

	function clear() {
		xmlfile.val('')
		$('.xml-file-name').text('')
		xsdfile.val('')
		$('.xsd-file-name').text('')
	}
});
